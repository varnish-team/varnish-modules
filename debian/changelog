varnish-modules (0.25.0-1) unstable; urgency=medium

  * New upstream release.

 -- Marco d'Itri <md@linux.it>  Mon, 21 Oct 2024 23:47:07 +0200

varnish-modules (0.24.0-2) unstable; urgency=medium

  * Disable the be_enabled test in the autopkgtest because it fails with
    the new Varnish package, which has no init script. (See #1074791.)

 -- Marco d'Itri <md@linux.it>  Wed, 03 Jul 2024 22:45:02 +0200

varnish-modules (0.24.0-1) unstable; urgency=medium

  * New upstream release.
  * Add myself as an Uploader.
  * Add support for loong64. (Closes: #1055145)

 -- Marco d'Itri <md@linux.it>  Sun, 30 Jun 2024 16:51:22 +0200

varnish-modules (0.20.0-2) unstable; urgency=medium

  * Remove architectures where varnish-modules do not build or pass build tests
    - Build tests fail on armel, armhf, sparc64
    - Build dependencies not available on hurd-i386, kfreebsd-*

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sat, 13 Aug 2022 10:05:00 +0200

varnish-modules (0.20.0-1) unstable; urgency=medium

  [ Stig Sandbeck Mathisen ]
  * New upstream release
  * Update varnish dependencies to depend on varnish 7.1
  * Declare compliance with Debian Policy Manual 4.6.1
  * Update debian/copyright

  * Import upstream commits after release tag
    [ Guillaume Quintard ]
    * release 0.20.0
    [ Dario Hamidi ]
    * fix(vmod_var): gracefully handle WS_Alloc failures

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sat, 18 Jun 2022 08:39:48 +0200

varnish-modules (0.18.0-1) unstable; urgency=medium

  * New upstream version
    * Drop patches included upstream
  * Set environment variable VARNISHAPI_DATAROOTDIR for autoreconf
  * Depend on Varnish ABI "strict" version (Closes: #991348)
  * Add source package configuration as recommended by dgit
  * Update standards version to 4.5.1, no changes needed.
  * Update build dependencies
    * Ensure varnish-modules 0.18.0 builds with varnish-6.6
    * Drop redundant build dependency on autotools-dev
  * Bump debhelper from deprecated 9 to 13. + Drop check for DEB_BUILD_OPTIONS
    containing "nocheck", since debhelper now does this.
  * Set debhelper-compat version in Build-Depends.
  * autopkgtest: Add dependency on net-tools for netstat
  * Add CI pipeline for salsa.debian.org

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sun, 19 Sep 2021 14:00:47 +0200

varnish-modules (0.16.0-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Backport upstream fix for FTBFS with Varnish 6.5.
    (Closes: #971142)

 -- Adrian Bunk <bunk@debian.org>  Tue, 10 Nov 2020 10:21:35 +0200

varnish-modules (0.16.0-2) unstable; urgency=medium

  * Fix FTBFS issue with Varnish ABI dependency (Closes: #963452)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Drop unnecessary dh arguments: --parallel

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Wed, 22 Jul 2020 11:11:28 +0200

varnish-modules (0.16.0-1) unstable; urgency=medium

  [ Jonas Genannt ]
  * New upstream version 0.16.0 (Closes: #939932)
  * debian/patches removed all patches, included into new upstream version
  * debian/NEWS: added information about missing cookie vmod
  * debian/control:
    * updated description (cookie removed)
    * bump varnish depends to 6.4.0
    * bumpd standards version
  * debian/rules: tarball has no bootstrap included
  * copyright: updated to new tarball

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Tue, 05 May 2020 19:48:59 +0200

varnish-modules (0.15.0-1) unstable; urgency=medium

  * Imported upstream release 0.15.0 (Closes: #910630)
    * Remove vmod "softpurge", no longer supported upstream.
    * Add vmods "bodyaccess" and "var".
  * Update packaging maintainer address (Closes: #899821)
  * Declare compliance with Debian policy 4.1.0
  * Import patches for 32bit and new ABI
  * Refresh existing patches
  * Update Vcs-* urls

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Mon, 24 Dec 2018 10:51:29 +0100

varnish-modules (0.12.1-1) unstable; urgency=medium

  * New upstream release (Closes: #853696)
  * Drop patch to support Varnish 5.0.0, included upstream
  * Set versioned build-dep on varnish/libvarnishapi-dev to 5.0.0
  * Empty dependency_libs field in .la files (c.f. Policy 10.2).
  * Declare compliance with Debian policy 4.1.0
  * Update debian/watch to follow upstream's new release tags naming
    convention

 -- Emanuele Rocca <ema@debian.org>  Wed, 04 Oct 2017 12:53:39 +0200

varnish-modules (0.9.1-4) unstable; urgency=medium

  * Add patch to support Varnish 5.0.0.
    Thanks to Dridi Boukelmoune (Closes: #838114)
  * Add hardening rules
  * Support DEB_BUILD_OPTIONS=nocheck
  * Install man pages

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Tue, 20 Sep 2016 14:35:35 +0200

varnish-modules (0.9.1-3) unstable; urgency=medium

  * 0001-no-cookie-overflow-test.patch: tests/cookie/08-overflow.vtc fails on
    32-bit arches. Do not expect insanely long cookie names to be supported.

 -- Emanuele Rocca <ema@debian.org>  Fri, 22 Jul 2016 10:52:11 +0200

varnish-modules (0.9.1-2) unstable; urgency=medium

  * varnishtest attempts to resolve phk.freebsd.dk to assess whether DNS
    resolution works. As network access during a build is against Debian
    Policy 4.9, use libnss-wrapper to avoid actual DNS requests in
    dh_auto_test (Closes: #830843)

 -- Emanuele Rocca <ema@debian.org>  Thu, 14 Jul 2016 10:11:27 +0200

varnish-modules (0.9.1-1) unstable; urgency=medium

  * New upstream release
  * Add versioned build-depend on varnish >= 4.1.3
  * Bump versioned build-depend on libvarnishapi1
  * Drop 0001-Use-a-size_t-format-string-for-value-from-strlen.patch
  * Add myself to the Uploaders field

 -- Emanuele Rocca <ema@debian.org>  Thu, 07 Jul 2016 17:27:59 +0200

varnish-modules (0.9.0-3) unstable; urgency=medium

  * Declare compliance with Debian policy 3.9.8
  * autopkgtest with serverspec

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Mon, 23 May 2016 23:31:01 +0200

varnish-modules (0.9.0-2) unstable; urgency=medium

  * [52c0d1e] Set team as maintainer
  * [1243692] Add Vcs-* URLs
  * [20b6824] Let "cme" fix things
  * [4513868] Import format string patch.
    Thanks to Aaron M. Ucko (Closes: #817169)

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Tue, 08 Mar 2016 20:16:00 +0100

varnish-modules (0.9.0-1) unstable; urgency=medium

  * Initial release (Closes: #816846)

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sun, 06 Mar 2016 10:47:30 +0100
